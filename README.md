
<div style="text-align:center"><img src="activos/imagenes/amor-infinito.jpg" width="222" height="123" /></div>

# <center><font color="red">A</font>mor</center>

*El **amor** es la manifestación divina de la **consciencia**,* <br/>
*así mismo, es la base para nuestra humanidad*. <br/>
Si hay ***amor*** y además...

* somos compartidos tendremos ***generosidad***.
* somos desinteresados tendremos ***humildad***.
* somos mesurados tendremos ***templanza***.
* tenemos educación seremos ***respetuosos***.
* tenemos interés seremos ***comprometidos***.
* tenemos caridad seremos ***diligentes***.
* tenemos templanza seremos ***castos***.
* tenemos calma seremos ***pacientes***.

Cuando se tiene un inmenso ***amor*** hacia alguien o algo parece como si todas las cosas o <br/>
situaciones *"malas"* se resolvieran solas o fácilmente y con poco esfuerzo, <br/>
incluso sin necesidad de tener moralidad, valores o ética.

Si somos ***conscientes*** ninguna de esas cosas se necesitan, <br/>
nos deberíamos volver personas ***conscientes*** o al menos amorosas. <br/>
Una vez que se tengamos eso cualquier decisión tomada será muy alegre y maravillosa.

Supongamos que eso es *"imposible"*, la siguiente mejor opción es *tener profundo sentido del **amor***, <br/>
si se tiene eso entonces los valores, la ética o cualquier otra cosa, será un bueno complemento, <br/>
porque lo que se ama siempre se cuidará de la mejor manera posible.

Generalmente las personas *"piensan"* que el ***amor*** es una plática cursi, <br/>
tonterías como: *yo te amo y tú me amas..., tú me quieres y yo te quiero..., etc*. <br/>
Si se cree que el ***amor*** es un plan de beneficio mutuo, entonces es otra cosa menos ***amor***.

Finalmente, ***el amor*** es realmente *un sentido muy profundo de **integridad** y **compromiso***, por eso <br/>
cuando se ama de verdad a algo o alguien se experimenta un enorme sentido de ***integridad*** <br/>
hacia eso y siempre, siempre se hará lo mejor posible para lo que se ama. <br/>

<div style="text-align:center"><img src="activos/imagenes/amor-infinito-3.jpg" width="333" height="66"/></div>
<p style="text-align:center;color:red">
El amor es más que unas palabras dulces<br/>
o caricias tiernas y emociones sensibleras.<br/>
<br/>
El amor es la base de la integridad, el valor y el sacrificio.<br/>
<br/>
Quien sino aquellos cuyos corazones están empapados de amor,<br/>
han antepuesto el bienestar de otros al suyo mismo.<br/>
<br/>
Quien se ha enfrentado a amenazas y peligros<br/>
sino aquellos dedicados al amor.<br/>
<br/>
Quien sino los amantes han estado más dispuestos<br/>
a sacrificar todo lo que importa y así mismos<br/>
en el altar del amor.<br/>
<br/>
El amor, el más delicado y el más resiliente<br/>
de todos los rasgos humanos.
</p>
